#!/bin/python

from pathlib import Path
from datetime import datetime
import tempfile
import json
import csv
import subprocess
import platform

# original file to start with
orig_file = Path("orig/test.mov")
# output-dir to place the encoded files
# leave empty to not safe the files
output_dir = Path("mp4_out")
# output-dir to place the csv-files with the results
csv_dir = Path("results")
# model of the vmaf_model that should be used
vmaf_model_path = Path("vmaf_models/vmaf_v0.6.1.json")
# number of threads to use (default (auto) = 0)
threads = "0"

# presets to encode
presets = ["ultrafast", "superfast", "veryfast", "faster", "fast", "medium", "slow", "slower", "veryslow"]

# store the results
results = {}

# modes to encode with their settings
modes = {
	"crf_19": {
		"c:v": "libx264",
		"b:v": "0",
		"crf": "19",
		"threads": threads
	},
	"b-v_15M": {
		"c:v": "libx264",
		"b:v": str(15 * pow(1000, 2)), # in kilobytes
		"threads": threads
	}
}

# different ffmpeg exec names for windows or linux
ffmpeg_exec = 'ffmpeg'
if platform.system() == 'Windows':
	ffmpeg_exec += '.exe'

def ffmpeg(input_files, args, output_file, overwrite_output=False):
	# list with all arguments to be sent to ffmpeg
	subprocess_call_args = [ffmpeg_exec]

	# add the input files to the list
	for in_file in input_files:
		subprocess_call_args.append("-i")
		subprocess_call_args.append(in_file)

	# add the arguments to tthe list
	for arg in args:
		subprocess_call_args.append("-{}".format(arg))
		subprocess_call_args.append(args[arg])

	if overwrite_output:
		subprocess_call_args.append("-y")

	subprocess_call_args.append(output_file)

	subprocess.run(subprocess_call_args)

# if the output dirs don't exist, create them
output_dir.mkdir(exist_ok=True, parents=True)
csv_dir.mkdir(exist_ok=True, parents=True)

# make all the encodings
for mode in modes: # go through the modes
	results[mode] = [] # create an empty list to append the results to

	for preset in presets: # go trought the presets
		if output_dir == "":
			export_file = tempfile.NamedTemporaryFile(suffix=".mp4")
			output_file = export_file
			export_file.close()

		else:
			output_file = Path("{}/{}.{}.mp4".format(output_dir, mode, preset))

		ffmpeg_args = modes[mode]
		ffmpeg_args["preset"] = preset

		bef = datetime.now() # create a timestamp before encoding

		ffmpeg([orig_file], modes[mode], output_file, overwrite_output=True)

		aft = datetime.now() # create a timestamp after encoding

		delta = aft-bef

		temp_json_path = output_dir / Path('vmaf_results.json')

		print ("starting vmaf")

		vmaf_args = {
			"lavfi": "libvmaf='log_fmt=json:log_path={}:model_path={}'".format(temp_json_path.as_posix(), vmaf_model_path.as_posix()),
			"f": "null",
			"threads": threads
		}

		input_files = [orig_file, output_file]

		ffmpeg(input_files, vmaf_args, '-')

		print (vmaf_args["lavfi"])

		with open(temp_json_path) as temp_json_file:
			data = temp_json_file.read()
			json_data = json.loads(data)

		temp_json_path.unlink()

		results[mode].append({
			"preset": preset,
			"time": delta.total_seconds(),
			"size": Path(output_file).stat().st_size / 1024 / 1024,
			"vmaf": json_data["pooled_metrics"]["vmaf"]["harmonic_mean"]
		})

		if output_dir == "":
			output_file.unlink

for mode in results:
	csv_columns = []
	for key in results[mode][0].keys():
		csv_columns.append(key)

	with open("{}/{}.csv".format(csv_dir, mode), 'w') as csv_file:
		writer = csv.DictWriter(csv_file, fieldnames=csv_columns)
		writer.writeheader()

		for data in results[mode]:
			writer.writerow(data)